@extends('main')

@section('miniOnContent')
<section class="vbox">
    <section class="hbox stretch">
        <aside class="aside-xl dk b-r" id="sidebar">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <form 
                    class="navbar-form navbar-left m-l-n-xs m-t hidden-xs" 
                    role="search" 
                    id="post-search" 
                    action="{{ route('admin.posts.search') }}" 
                    method="POST">
                        <input name="_token" type="hidden" value="{{ csrf_token() }}">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-btn">
                                  <button type="submit" class="btn bg-white btn-icon rounded"><i class="fa fa-search"></i></button>
                                </span>
                                {!! $form->text('query', null, ['class' => 'form-control  no-border rounded', 'placeholder'=> 'Search title...']) !!}
                                <span class="input-group-btn">
                                   <a class="btn bg-white btn-icon rounded" href="{{ route('admin.posts.create') }}" data-target="#bjax-target" id="create-post"><i class="icon icon-pencil"></i></a>
                                </span>
                            </div>
                        </div>
                    </form>
                </div>
            </div>            
            <section class="vbox">
                <section class="scrollable hover">
                    <ul id="lists" data-path="{{ route('admin.posts.data', ['time'=> time() ]) }}" data-template="#list-posts" class="list-group list-group-lg no-bg auto m-b-none no-border b-b m-t-n-xxs lists"></ul>
                </section>
            </section>
        </aside>

        <section>
            <section class="vbox">
                <section class="scrollable wrapper-sm" id="bjax-target">
                    {{-- ajax load contents --}}
                </section>
            </section>  
        </section>    
    </section>
</section>
@endsection

@section('js')

@endsection