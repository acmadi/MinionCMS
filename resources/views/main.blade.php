@extends('layout')

@section('mainContent')
<section class="vbox">
	@include('partials.header')
  <section>
    <section class="hbox stretch">
      @include('partials.menu')
      <section id="content">
        @yield('miniOnContent')
      </section>
    </section>
  </section>
</section>
@endsection