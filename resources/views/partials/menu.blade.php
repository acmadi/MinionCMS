<aside class="bg-white aside hidden-print b-r" id="nav">
  {{-- <nav class="nav-primary hidden-xs b-b">
    <ul class="nav">
      <li>
        <a href="{{ route('admin.index') }}">
          <i class="icon-calendar icon"></i>
          <span>{{ date('d/m/Y H:i') }}</span>
        </a>
      </li>
    </ul>
  </nav> --}}
  <footer class="footer hidden-xs no-padder text-center-nav-xs b-b">
    <div class="bg hidden-xs m-b-n-xxs">
      <div class="dropdown wrapper-sm clearfix">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
          <span class="thumb-sm avatar pull-left m-l-xs">                        
            <img src="/images/a3.png" class="dker" alt="...">
          </span>
          <span class="hidden-nav-xs clear">
            <span class="block m-l">
              <strong class="font-bold text-lt">John.Smith</strong> 
              <b class="caret"></b>
            </span>
            <span class="text-muted text-xs block m-l">Art Director</span>
          </span>
        </a>
        <ul class="dropdown-menu animated fadeInRight aside text-left">
          <li>
            <span class="arrow top hidden-nav-xs"></span>
            <a href="#">Settings</a>
          </li>
          <li>
            <a href="profile.html">Profile</a>
          </li>
          <li>
            <a href="#">
              <span class="badge bg-danger pull-right">3</span>
              Notifications
            </a>
          </li>
          <li>
            <a href="docs.html">Help</a>
          </li>
          <li class="divider"></li>
          <li>
            <a href="{{ route('auth.logout') }}">Logout</a>
          </li>
        </ul>
      </div>
    </div>            
  </footer>         
  <section class="vbox">
    <section class="scrollable">
      <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-railOpacity="0.2">
        <!-- nav -->                 
        <nav class="nav-primary hidden-xs">
          <ul class="nav" data-ride="collapse">
            <li class="divider"></li>
            <li>
              <a href="{{ route('admin.index') }}">
                <i class="icon-speedometer icon"></i>
                <span>Dashboard</span>
              </a>
            </li>
            <li >
              <a href="#" class="auto">
                <span class="pull-right text-muted">
                  <i class="fa fa-angle-left text"></i>
                  <i class="fa fa-angle-down text-active"></i>
                </span>
                <i class="icon-pin icon">
                </i>
                <span>Posts</span>
              </a>
              <ul class="nav dk text-sm bg-white">
                <li >
                  <a href="{{ route('admin.posts.index') }}" class="auto">
                    <i class="fa fa-angle-right text-xs"></i>
                    <span>All Posts</span>
                  </a>
                </li>
                <li >
                  <a href="#" class="auto">
                    <i class="fa fa-angle-right text-xs"></i>
                    <span>Categories</span>
                  </a>
                </li>
                <li >
                  <a href="#" class="auto">
                    <i class="fa fa-angle-right text-xs"></i>
                    <span>Tags</span>
                  </a>
                </li>
              </ul>
            </li>
            <li >
              <a href="#" class="auto">
                <span class="pull-right text-muted">
                  <i class="fa fa-angle-left text"></i>
                  <i class="fa fa-angle-down text-active"></i>
                </span>
                <i class="icon-picture icon">
                </i>
                <span>Media</span>
              </a>
              <ul class="nav dk text-sm bg-white">
                <li >
                  <a href="{{ route('media.index') }}" class="auto">                               
                    <i class="fa fa-angle-right text-xs"></i>
                    <span>Library</span>
                  </a>
                </li>
                <li >
                  <a href="#" class="auto">                             
                    <i class="fa fa-angle-right text-xs"></i>
                    <span>Add New</span>
                  </a>
                </li>
              </ul>
            </li>
            <li >
              <a href="#" class="auto">
                <span class="pull-right text-muted">
                  <i class="fa fa-angle-left text"></i>
                  <i class="fa fa-angle-down text-active"></i>
                </span>
                <i class="icon-grid icon">
                </i>
                <span>Page</span>
              </a>
              <ul class="nav dk text-sm bg-white">
                <li >
                  <a href="#" class="auto">                               
                    <i class="fa fa-angle-right text-xs"></i>
                    <span>All Pages</span>
                  </a>
                </li>
                <li >
                  <a href="#" class="auto">                             
                    <i class="fa fa-angle-right text-xs"></i>
                    <span>Add New</span>
                  </a>
                </li>
              </ul>
            </li>
            <li >
              <a href="#" class="auto">
                <span class="pull-right text-muted">
                  <i class="fa fa-angle-left text"></i>
                  <i class="fa fa-angle-down text-active"></i>
                </span>
                <i class="icon-puzzle icon">
                </i>
                <span>Plugins</span>
              </a>
              <ul class="nav dk text-sm bg-white">
                <li >
                  <a href="#" class="auto">                               
                    <i class="fa fa-angle-right text-xs"></i>
                    <span>All Plugins</span>
                  </a>
                </li>
                <li >
                  <a href="#" class="auto">                             
                    <i class="fa fa-angle-right text-xs"></i>
                    <span>Settings</span>
                  </a>
                </li>
              </ul>
            </li>
            <li >
              <a href="#" class="auto">
                <span class="pull-right text-muted">
                  <i class="fa fa-angle-left text"></i>
                  <i class="fa fa-angle-down text-active"></i>
                </span>
                <i class="icon-users icon">
                </i>
                <span>Users</span>
              </a>
              <ul class="nav dk text-sm bg-white">
                <li >
                  <a href="#" class="auto">                               
                    <i class="fa fa-angle-right text-xs"></i>
                    <span>All Users</span>
                  </a>
                </li>
                <li >
                  <a href="#" class="auto">                             
                    <i class="fa fa-angle-right text-xs"></i>
                    <span>Add New</span>
                  </a>
                </li>
              </ul>
            </li>
            <li >
              <a href="#" class="auto">
                <span class="pull-right text-muted">
                  <i class="fa fa-angle-left text"></i>
                  <i class="fa fa-angle-down text-active"></i>
                </span>
                <i class="icon-settings icon">
                </i>
                <span>Settings</span>
              </a>
              <ul class="nav dk text-sm bg-white">
                <li >
                  <a href="#" class="auto">                               
                    <i class="fa fa-angle-right text-xs"></i>
                    <span>General</span>
                  </a>
                </li>
                <li >
                  <a href="#" class="auto">                             
                    <i class="fa fa-angle-right text-xs"></i>
                    <span>Media</span>
                  </a>
                </li>
                <li >
                  <a href="#" class="auto">                             
                    <i class="fa fa-angle-right text-xs"></i>
                    <span>Themes</span>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </nav>
        <!-- / nav -->
      </div>
    </section>
    
  </section>
</aside>