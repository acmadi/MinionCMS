@extends('main')

@section('miniOnContent')
<section class="vbox">
	<section id="bjax-target">
		<section class="hbox stretch">
			<!-- side content -->
			<aside class="aside-xl b-r bg-white" id="sidebar">
        <div class="sub-nav min-one b-b">
         	<form role="search" id="post-search" action="{{ route('admin.posts.search') }}" method="POST" class="search">
            <input name="_token" type="hidden" value="{{ csrf_token() }}">
            <div class="form-group">
  		        <div class="input-group">
              	<span class="input-group-btn">
                  <button type="submit" class="btn bg-white btn-icon rounded"><i class="fa fa-search"></i></button>
                </span>
                {!! $form->text('query', null, ['class' => 'form-control no-border rounded form-search b-r', 'placeholder'=> 'Search title...']) !!}                
  			      </div>
            </div>
          </form>
        </div> 
        <section class="vbox animated fadeInUp">
          <section class="scrollable hover w-f-md">
            
          </section>
        </section>
			</aside><!-- / side content -->
                
			<aside class="bg-white">
        {{-- <header class="header text-right underline-tab">
        	<ul class="nav nav-tabs pull-left">
          	<li class="active"><a href="#form" data-toggle="tab">DATA-FORM</a></li>
          	<li class=""><a href="#view" data-toggle="tab">DATA-VIEW</a></li>
        	</ul>
        </header>   --}}
        <div class="sub-nav b-b">
          <div class="row">
            <div class="col-md-8">
              <h5 class="title">MiniON Dashboard</h5>
            </div>
          </div>
        </div>
        <section class="vbox">
          <section class="scrollable w-f-md">
            
            <div class="row">
              <div class="col-sm-6 portlet">
                <section class="panel panel-info portlet-item">
                  <header class="panel-heading">
                    Quick link
                  </header>
                  <div class="list-group bg-white">
                    <a href="#" class="list-group-item">
                      <i class="fa fa-fw fa-envelope"></i> Inbox
                    </a>
                    <a href="#" class="list-group-item">
                      <i class="fa fa-fw fa-eye"></i> Profile visits
                    </a>
                    <a href="#" class="list-group-item">
                      <i class="fa fa-fw fa-phone"></i> Call
                    </a>
                    <a href="#" class="list-group-item">
                      <i class="fa fa-fw fa-comments-o"></i> Messages
                    </a>
                    <a href="#" class="list-group-item">
                      <i class="fa fa-fw fa-bookmark"></i> Bookmarks
                    </a>
                    <a href="#" class="list-group-item">
                      <i class="fa fa-fw fa-bell"></i> Notifications
                    </a>
                  </div>
                </section>
                <section class="panel panel-default portlet-item">
                  <header class="panel-heading">
                    <ul class="nav nav-pills pull-right">
                      <li>
                        <a href="#" class="panel-toggle text-muted"><i class="fa fa-caret-down text-active"></i><i class="fa fa-caret-up text"></i></a>
                      </li>
                    </ul>
                    News <span class="badge bg-info">32</span>                    
                  </header>
                  <section class="panel-body">
                    <article class="media">
                      <div class="pull-left">
                        <span class="fa-stack fa-lg">
                          <i class="fa fa-circle fa-stack-2x"></i>
                          <i class="fa fa-bold fa-stack-1x text-white"></i>
                        </span>
                      </div>
                      <div class="media-body">                        
                        <a href="#" class="h4">Bootstrap 3: What you need to know</a>
                        <small class="block m-t-xs">Sleek, intuitive, and powerful mobile-first front-end framework for faster and easier web development.</small>
                        <em class="text-xs">Posted on <span class="text-danger">Feb 23, 2013</span></em>
                      </div>
                    </article>
                    <div class="line pull-in"></div>
                    <article class="media">
                      <div class="pull-left">
                        <span class="fa-stack fa-lg">
                          <i class="fa fa-circle fa-stack-2x text-info"></i>
                          <i class="fa fa-file-o fa-stack-1x text-white"></i>
                        </span>
                      </div>
                      <div class="media-body">
                        <a href="#" class="h4">Bootstrap documents</a>
                        <small class="block m-t-xs">There are a few easy ways to quickly get started with Bootstrap, each one appealing to a different skill level and use case. Read through to see what suits your particular needs.</small>
                        <em class="text-xs">Posted on <span class="text-danger">Feb 12, 2013</span></em>
                      </div>
                    </article>
                    <div class="line pull-in"></div>
                    <article class="media">
                      <div class="pull-left">
                        <span class="fa-stack fa-lg">
                          <i class="fa fa-circle fa-stack-2x icon-muted"></i>
                          <i class="fa fa-mobile fa-stack-1x text-white"></i>
                        </span>
                      </div>
                      <div class="media-body">
                        <a href="#" class="h4 text-success">Mobile first html/css framework</a>
                        <small class="block m-t-xs">Bootstrap, Ratchet</small>
                        <em class="text-xs">Posted on <span class="text-danger">Feb 05, 2013</span></em>
                      </div>
                    </article>
                  </section>
                </section>
              </div>

          </section>
        </section>
      </aside>
      
		</section>
	</section>
</section>
<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen,open" data-target="#nav,html"></a>
@endsection

@section('js')
<script>
$(document).ready(function($) {
	$('#lists').renderTemplate({
        after: function(){
        },
        replace: true 
    });
});
</script>
@endsection