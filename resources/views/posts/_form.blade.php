<div class="form-group">
    <label>Title</label>
    {!! $form->text('post_title', null, ['class' => 'form-control', 'placeholder'=> trans('form.enter_title'), 'id' => 'post_title' ]) !!}
</div>
<p class="text-muted">{{ url('/') }}/<span>{{ 'test-slug' }}</span></p>
<br>
<div class="form-group">
    {!! $form->textarea('post_content', null, ['class' => 'form-control tinymce', 'rows' => 10]) !!}
</div>
<div class="form-group">
    <label>Category</label>
    <?php
        $categories = $taxonomy->category()->get();
        $allcategory = [];
        foreach($categories as $category)
        {
            $allcategory[$category->term->name] = $category->term->name; 
        }
    ?>
    {!! $form->select('category[]', $allcategory, null, ['class' => 'form-control tags-format', 'multiple' => true]) !!}
</div>
<div class="form-group">
    <label>Tags</label>
    <?php
        $tags = $taxonomy->tags()->get();
        $alltags = [];
        foreach($tags as $tag)
        {
            $alltags[$tag->term->name] = $tag->term->name; 
        }
    ?>
    {!! $form->select('tags[]', $alltags, null, ['class' => 'form-control tags-format', 'multiple' => true ]) !!}
</div>
<div class="form-group pull-in clearfix">
    <div class="col-sm-4">
        <label>Discussion</label>
        <div class="checkbox i-checks">
            <label>
                {!! $form->checkbox('comment_status', 'open', true) !!} 
                <i></i> Allow comments
            </label>
        </div>
    </div>
    <div class="col-sm-8">
        <label>&nbsp;</label>
        <div class="checkbox i-checks">
            <label>
                <input type="checkbox" value="">
                <i></i> Allow trackbacks and pingbacks on this page. 
            </label>
        </div>                
    </div>   
</div>
<div class="form-group">
    <label>Excerpt</label>
    {!! $form->textarea('post_excerpt', null, ['class' => 'form-control', 'rows' => 2]) !!}
</div>