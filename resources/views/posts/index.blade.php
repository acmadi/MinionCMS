@extends('main')

@section('miniOnContent')
<section class="vbox">
    <section id="main-target">
        <section class="hbox stretch">
            
            <aside class="aside-xl b-r bg-white" id="sidebar">
                {{-- <header class="header text-right underline-tab">
                    <ul class="nav nav-tabs pull-left">
                        <li class="active"><a href="#">POST LIST</a></li>
                    </ul>
                    <span class="hidden-sm"><i class="icon icon-trash"></i></span>
                </header> --}}
                <div class="sub-nav min-one b-b">
                    <form role="search" id="post-search" action="{{ route('admin.posts.search') }}" method="POST" class="search">
                    <input name="_token" type="hidden" value="{{ csrf_token() }}">
                    <div class="form-group">
                        <div class="input-group">
                        <span class="input-group-btn">
                          <button type="submit" class="btn bg-white btn-icon rounded"><i class="fa fa-search"></i></button>
                        </span>
                        {!! $form->text('query', null, ['class' => 'form-control no-border rounded form-search', 'placeholder'=> 'Search title...']) !!}
                        <span class="input-group-btn">
                                  <a class="btn bg-white btn-icon rounded" href="{{ route('admin.posts.create') }}" data-target="#bjax-target" id="create-post"><i class="icon icon-pencil"></i></a>
                        </span>
                          </div>
                    </div>
                  </form>
                </div> 
                <section class="vbox animated fadeInUp">
                  <section class="scrollable hover w-f-md">
                    <ul id="lists" data-path="{{ route('admin.posts.data', ['time'=> time() ]) }}" data-template="#list-posts" class="list-group list-group-lg no-bg auto m-b-none no-border m-t-n-xxs lists"></ul>
                  </section>
                </section>
            </aside><!-- / side content -->
            
            <aside class="bg-white" id="content-form">
                {{-- <header class="header text-right underline-tab">
                    <ul class="nav nav-tabs pull-left">
                        <li class="active"><a href="#form" data-toggle="tab">DATA-FORM</a></li>
                        @if(isset($post))
                        <li class=""><a data-toggle="tabajax" href="{{ route('admin.posts.show', ['id' => $post->ID]) }}" data-target="#two-target">DATA-VIEW</a></li>
                        @endif
                    </ul>
                </header>  --}} 
                <div class="sub-nav b-b">
                    <div class="row">
                        <div class="col-md-8">
                            <h5 class="title">POST FORM</h5>
                        </div>
                    </div>
                </div>
                <section class="vbox">
                    <section class="scrollable w-f-md wrapper">
                        
                        <div class="wellcome-logo">
                            <div class="logo">
                                <i class="icon icon-screen-desktop"></i>
                                <h4>miniOn CMS</h4>
                                <span>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                </span>
                                
                            </div>
                        </div>

                    </section>
                </section>
            </aside>    
        </section>
    </section>
</section>
<a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen,open" data-target="#nav,html"></a>
@endsection

@section('js')
<script>
    function bindItemClick(){
        $('.lists li > a').off('click').on('click', function(e){
            $(this).bjax({
                url: $(this).attr('href') || $(this).attr('data-url'),
                target: '#content-form',
                callback: function(){
                    
                    $('#post-form').ajaxForm({
                        success: function(data){
                            notyf('success', data.messages);
                            renderData();
                        },
                        beforeSerialize: function(){
                            tinyMCE.triggerSave();
                        }
                    });
                   
                    $('.tags-format').select2({
                        tags: true,
                        tokenSeparators: [',']
                    });
                    // load texteditor
                    initTinymce();
                }
            });
            e.preventDefault();
        });
    }
    $('#create-post').off('click').on('click', function(e){
        $(this).bjax({
            url: $(this).attr('href') || $(this).attr('data-url'),
            target: '#content-form',
            callback: function(){                            
                $('#post-form').ajaxForm({
                    success: function(data){
                        notyf('success', data.messages);
                        $('#content-form').html('');
                        renderData();
                    },
                    beforeSerialize: function(){
                        tinyMCE.triggerSave();
                    }
                });
                $('.tags-format').select2({
                    tags: true,
                    tokenSeparators: [',']
                });
                // load texteditor
                initTinymce();
            }
        });
        e.preventDefault();
    });
    function renderData()
    {
        $('#lists').renderTemplate({
            after: function(){
                bindItemClick();
            },
            replace: true 
        });
    }    
    $(document).ready(function(){
        $('#post-search').ajaxForm({
            success: function(data){
                $('#lists').html(data.data);
                bindItemClick();
            }
        });
        renderData();
    });

    function notyf(type, messages)
    {
        noty({
            text        : messages,
            type        : type,
            dismissQueue: true,
            layout      : 'topRight',
            theme       : 'relax',
            timeout     : 700
        });
    }


</script>

@endsection