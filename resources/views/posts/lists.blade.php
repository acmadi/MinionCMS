@foreach($posts as $row)
<li class="list-group-item clearfix">
  <a href="{{ route('admin.posts.comments', ['id' => $row->ID]) }}" class="pull-right m-l text-md">
    <i class="icon icon-ban text"></i>
  </a>
  <a href="{{ route('admin.posts.edit', ['id' => $row->ID]) }}" class="pull-left thumb-ls m-r">
    @if(is_null($row->primary_image))
      <img src="{{ route('image', ['path' => 'images/p0.jpg', 'size' => '58x58']) }}">
    @else
      <img src="{{ route('image', ['path' => 'uploads/'.$row->primary_image, 'size' => '58x58']) }}">
    @endif
  </a>
  <a class="clear" href="{{ route('admin.posts.edit', ['id' => $row->ID]) }}">
    <span class="block text-ellipsis">{{ $row->post_title }}</span>
    <small class="text-muted block">
      {{ implode(', ', $row->categoryNames()) }}
    </small>
    <small class="text-muted block">
      {{ $row->post_date }} {{ ucfirst($row->post_status) }}, {{ $row->author->display_name }}
    </small>
  </a>
</li>
@endforeach