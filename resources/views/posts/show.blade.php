	<div class="post-media">
    	<img src="/images/m42.jpg" class="img-full">
	</div>
    <div class="caption">
		<h2 class="post-title">
			<a href="#">{{ $post->post_title }}</a>
		</h2>
        <div class="post-sum">
			{!! $post->post_content !!}
        </div>
		<div class="line line-lg"></div>
            <div class="text-muted">
				<i class="fa fa-user icon-muted"></i> by <a href="#" class="m-r-sm">Admin</a>
              	<i class="fa fa-clock-o icon-muted"></i> Feb 20, 2013
              	<a href="#" class="m-l-sm"><i class="fa fa-comment-o icon-muted"></i> 2 comments</a>
            </div>
       	</div>
	</div>