<div class="sub-nav b-b">
    <div class="row">
        <div class="col-md-8">
            <h5 class="title">POST FORM</h5>
        </div>
        <div class="col-md-4 text-right">
            <button class="btn btn-default btn-warning"><i class="icon icon-paper-clip"></i></button>
            <button class="btn btn-default btn-info"><i class="icon icon-picture"></i></button>
            <button type="submit" form="post-form" class="btn btn-default btn-success"><i class="fa fa-save"></i></button>
            <button class="btn btn-default btn-danger"><i class="icon icon-trash"></i></button>
        </div>
    </div>
</div>
<section class="vbox">
    <section class="scrollable w-f-md wrapper">
                
		<form action="{{ route('admin.posts.store') }}" method="POST" id="post-form">
			<input name="_token" type="hidden" value="{{ csrf_token() }}">
			
			@include('posts._form')
			
		</form>

    </section>
</section>
