/**
 * Minion CMS admin js
 */

function MiniOnStrore()
{
	var obj = $('#minion-post');

	obj.off('submit');
	obj.on('submit', function(e){
		e.preventDefault();

		var path = $(this).attr('action');
		var form = document.querySelector('#hicms-post');
		var data = new FormData(form);
		
		if (obj.find('textarea').length > 0){
			var name = $('textarea').attr('name');
			data.append(name, $('textarea').val()); }

		if (obj.find('input:file').length > 0){
			var name = $('input:file').attr('name'); }

		$.ajax({
			url: path,
			type: 'POST',
			data: data,
			processData: false,
  			contentType: false,
  			beforeSend: function(){
  				NProgress.start();
  			}
		})
		.done(function(rest) {
			
			swal({
				title: 'Success!', 
				text: 'Data was success to save on server', 
				type: 'success',
				timer: 2000,
				showConfirmButton: false
			});
			NProgress.done();
			location.reload();
		})
		.fail(function(rest) {
			
			swal({
				title: 'Failed!', 
				text: rest.msg,
				type: 'warning',
				timer: 2000,
				showConfirmButton: false
			});
			NProgress.done();
		});
	});
}