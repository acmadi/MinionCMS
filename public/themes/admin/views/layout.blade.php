<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>title</title>
    <meta name="keywords" content="keywords">
    <meta name="description" content="description">
    

    <link rel="stylesheet" href="{{ Theme::asset('admin::js/vendor/bootstrap/dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ Theme::asset('admin::js/vendor/metisMenu/dist/metisMenu.min.css') }}">
    <link rel="stylesheet" href="{{ Theme::asset('admin::js/vendor/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ Theme::asset('admin::js//vendor/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css') }}">
    <link rel="stylesheet" href="{{ Theme::asset('admin::css/sb-admin-2.css') }}">
    <link rel="stylesheet" href="{{ Theme::asset('admin::css/nprogress.css') }}">
    <link rel="stylesheet" href="{{ Theme::asset('admin::css/sweetalert.css') }}">
    <link rel="stylesheet" href="{{ Theme::asset('admin::css/admin-minioncms.css') }}">
    
    <!-- Load for tinymce -->
    <script type="text/javascript" src="{{ config('tinymce.cdn') }}"></script>
    <script type="text/javascript">
        tinymce.init(
            {!! json_encode(config('tinymce.params')) !!}
        );
    </script>
    
</head>
<body>
    
    @if(Auth::check())
    <div id="wrapper">
        
        @include('partials.sidebar')

        <!-- Page Content -->
            <div id="page-wrapper">
                    <div class="row">
                        <div class="col-lg-12">
                                
                            @yield('content')

                        </div>
                        <!-- /.col-lg-12 -->
                    </div>
                    <!-- /.row -->                
            </div>
            <!-- /#page-wrapper -->        
    </div>
    @else
    
        @yield('content')

    @endif
    

    <!-- Load JS -->
	<script src="/vendor/jquery/dist/jquery.min.js"></script>
    <script src="{{ Theme::asset('admin::js/vendor/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ Theme::asset('admin::js/vendor/metisMenu/dist/metisMenu.min.js') }}"></script>
    <script src="{{ Theme::asset('admin::js/vendor/moment/min/moment.min.js') }}"></script>
    <script src="{{ Theme::asset('admin::js/vendor/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ Theme::asset('admin::js/nprogress.min.js') }}"></script>
    <script src="{{ Theme::asset('admin::js/sweetalert.min.js') }}"></script>
    <script src="{{ Theme::asset('admin::js/sb-admin-2.js') }}"></script>
    <script src="{{ Theme::asset('admin::js/minion-admin.js') }}"></script>
    @yield('js')
</body>
</html>