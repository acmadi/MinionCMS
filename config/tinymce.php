<?php

return [

	'cdn' => url('vendor/tinymce/tinymce.min.js'),

	'params' => [
		"selector" => ".tinymce",
		"language" => 'id',
		"theme" => "modern",
		"skin" => "light",
		"height" => 400,
		"content_css" => url("vendor/tinymce/custom_content.css"),
		"plugins" => [
	         "advlist autolink link image lists charmap print preview hr anchor pagebreak ",
	         "wordcount visualblocks visualchars code fullscreen media nonbreaking",
	         "save contextmenu directionality emoticons template paste textcolor"
		],
		"menubar" => false,
		"end_container_on_empty_block" => true,
		"toolbar" => "insertfile | bold italic blockquote | alignleft aligncenter alignright alignjustify | pagebreak bullist numlist outdent indent | link image code",
	]

];