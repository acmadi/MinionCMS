<?php



return array(

	/*
	|--------------------------------------------------------------------------
	| Default Image Driver
	|--------------------------------------------------------------------------
	|
	| This option controls the default image "driver" used by Imagine library
	| to manipulate images.
	|
	| Supported: "gd", "imagick", "gmagick"
	|
	*/
	'driver' => 'gd'

);
