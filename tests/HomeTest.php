<?php

class HomeTest extends TestCase
{   
    
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testGetIndex()
    {
        $this->visit('/test')
             ->see('Home');
    }

    
}
