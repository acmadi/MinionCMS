<?php

$app->group(['prefix' => 'media', 'namespace' => 'Plugins\Media\Http\Controllers'], function($app)
{
	$app->get('/', ['uses' => 'MediaController@index', 'as' => 'media.index']);
	$app->get('{id}', ['uses' => 'MediaController@show', 'as' => 'media.show']);
});