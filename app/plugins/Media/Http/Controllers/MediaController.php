<?php 

namespace Plugins\Media\Http\Controllers;

use Minion\Entities\Post;
use Illuminate\Http\Request;
use Minion\Http\Controllers\Controller;

class MediaController extends Controller 
{
	protected $posts;

	public function __construct(Post $posts)
	{
		$this->posts = $posts;
	}

	public function index(Request $request)
	{
		$data = $this->posts->type('attachment')->latest('ID')->get();
		$hasItem = $request->get('item', null);
		if($request->has('item'))
		{
			$attachment = $this->posts->findOrFail($request->get('item'));
			$previous = $this->posts->type('attachment')->where('id', '<', $attachment->ID)->max('ID');
			$next = $this->posts->type('attachment')->where('id', '>', $attachment->ID)->min('ID');
			return view('media::index', compact('data','hasItem', 'attachment', 'previous', 'next'));
		}
		return view('media::index', compact('data','hasItem'));
	}
	
	public function show($id)
	{
		$data = $this->posts->findOrFail($id);
		
		return view('media::modal', compact('data', 'previous', 'next'));
	}
}