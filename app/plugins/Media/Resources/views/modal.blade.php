<div class="modal-dialog">
  <div class="modal-content">
    <div class="modal-header">
      <div class="modal-toolbar">
        <div class="item">
          <a href="{{ route('media.show',['id' => $next]) }}" class=""><i class="fa fa-angle-left"></i></a>
        </div>
        <div class="item">
          <a href="{{ route('media.show',['id' => $previous]) }}" class=""><i class="fa fa-angle-right"></i></a>
        </div>
        <div class="item">
          <a href="#" class="media-modal-close" data-dismiss="modal"><i class="fa fa-times"></i></a>
        </div>
      </div>
      <h4 class="modal-title">Attachments Details</h4>
    </div>
    <section class="vbox modal-body">
      <section class="hbox stretch">
        <aside class="b-r wrapper">
          <img src="{{ route('image', ['path' => 'uploads/'.$data->meta->_wp_attached_file, 'size' => '705x500', 'op'=> 'r']) }}"> 
        </aside>
        <aside class="aside-xxl wrapper">
          
        </aside>
      </section>    
    </section>
  </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->