<div class="row row-media">
  @foreach($data as $row)
  <?php 
    $meta = null;
    if(in_array($row->post_mime_type, ['image/jpeg', 'image/png'])){
      $meta = unserialize($row->meta->_wp_attachment_metadata);
    }
  ?>
  <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
    <div class="item">
      <div class="pos-rlt">
        @if(isset($meta))
          {{-- <img src="{{ url('img/'.$row->meta->_wp_attached_file.'?size=158x158') }}" alt="" class="img-full"> --}}
          <img src="{{ route('image', ['path' => 'uploads/'.$row->meta->_wp_attached_file, 'size' => '158x158']) }}" alt="" class="img-full">
        @else
          <div class="center text-center m-t-n-xxxl">
            <i class="icon icon-doc i-5x"></i>
          </div>
        @endif
        <div class="item-overlay r r-2x">
          <div class="center text-center m-t-n-lg">
            <a href="{{ route('media.index', ['item'=>$row->ID]) }}" data-bjax><i class="icon icon-frame i-2x"></i></a>
          </div>
        </div>
        <div class="meta bottom bg-black opacity05">
          <a href="#" class="text-ellipsis">{{ $row->post_title }}</a>
          <a href="#" class="text-ellipsis text-xs text-muted">{{ isset($meta)? $meta['width'].'x'.$meta['height']: $row->post_mime_type }} </a>
        </div>
      </div>
    </div>
  </div>
  @endforeach
</div>