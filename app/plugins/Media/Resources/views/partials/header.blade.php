<div class="sub-nav b-b">
	<div class="row media">
		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-left">
			
		    <a class="btn btn-default" href="{{ url('modal') }}" data-toggle="ajaxModal">
				<i class="icon icon-cloud-upload text"></i>
			</a>

		</div>
		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-center">
			<a class="btn btn-default">
				<i class="icon icon-grid text"></i>
			</a>
			<a class="btn btn-default">
				<i class="fa fa-th-list text"></i>
			</a>
		</div>
		<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 text-right">
			<a class="btn btn-default">
				<i class="icon icon-doc text"></i>
			</a>
			<a class="btn btn-default">
				<i class="icon icon-picture text"></i>
			</a>
			<a class="btn btn-default">
				<i class="icon icon-drawer text"></i>
			</a>
			<a class="btn btn-default">
				<i class="icon icon-film text"></i>
			</a>
			<a class="btn btn-default">
				<i class="icon icon-music-tone-alt text"></i>
			</a>

		</div>
	</div>	
</div>