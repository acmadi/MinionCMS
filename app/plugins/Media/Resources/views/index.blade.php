@extends('main')

@section('miniOnContent')
<section class="vbox bg-white">
    <section id="main-target">
		@include('media::partials.header')
		<section class="vbox">
			<section class="scrollable w-f-md wrapper">
				@include('media::partials.thumb')
			</section>
		</section>	
    </section>
</section>
@endsection

@section('minion-modal')
@if(isset($hasItem) && isset($attachment))
<div class="modal" aria-hidden="false" style="display: block;">
	<div class="modal-dialog">
	  <div class="modal-content">
	    <div class="modal-header">
	      <div class="modal-toolbar">
	        <div class="item">
	          <a class="" href="{{ route('media.index', ['item' => $next]) }}" data-bjax><i class="fa fa-angle-left"></i></a>
	        </div>
	        <div class="item">
	          <a class="" href="{{ route('media.index', ['item' => $previous]) }}" data-bjax ><i class="fa fa-angle-right"></i></a>
	        </div>
	        <div class="item">
	          <a href="{{ route('media.index') }}" data-bjax data class="media-modal-close" data-dismiss="modal"><i class="fa fa-times"></i></a>
	        </div>
	      </div>
	      <h4 class="modal-title">Attachments Details</h4>
	    </div>
	    <section class="vbox modal-body">
	      <section class="hbox stretch">
	        <aside class="b-r wrapper">
	          <img src="{{ route('image',  ['path' => 'uploads/'.$attachment->meta->_wp_attached_file, 'size' => '600x500', 'op'=>'r']) }}"> 
	          {{-- <img src="{{ url('uploads/'.$attachment->meta->_wp_attached_file) }}" class="img-modal"> --}}
	        </aside>
	        <aside class="aside-xxl wrapper">
	          
	        </aside>
	      </section>    
	    </section>
	  </div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div>
<div class="backdrop bg-black in"></div>
@endif
@endsection

