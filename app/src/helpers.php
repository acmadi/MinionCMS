<?php

/**
 *  Minion helpers 
 *  By: aditans88@gmail.com
 */

use Illuminate\Container\Container;

if (!function_exists('config_path')) {
    /**
     * Get the configuration path.
     *
     * @param  string  $path
     * @return string
     */
    function config_path($path = '')
    {   
        return app()->basePath('config').($path ? DIRECTORY_SEPARATOR.$path : $path);
        //return app()->make('path.config').($path ? DIRECTORY_SEPARATOR.$path : $path);
    }
}

if (!function_exists('storage_path')) {
    /**
     * Get the configuration path.
     *
     * @param  string  $path
     * @return string
     */
    function storage_path($path = '')
    {   
        return app()->basePath('storage').($path ? DIRECTORY_SEPARATOR.$path : $path);
        //return app()->make('path.config').($path ? DIRECTORY_SEPARATOR.$path : $path);
    }
}

if (!function_exists('public_path')) {
    /**
     * Get the path to the public folder.
     *
     * @param  string  $path
     * @return string
     */
    function public_path($path = '')
    {   
        return app()->basePath('public').($path ? DIRECTORY_SEPARATOR.$path : $path);
        //return app()->make('path.public').($path ? DIRECTORY_SEPARATOR.$path : $path);
    }
}

if (!function_exists('app_path')) {
    /**
     * Get the path to the application folder.
     *
     * @param  string  $path
     * @return string
     */
    function app_path($path = '')
    {
        return app()->basePath('app/src').($path ? DIRECTORY_SEPARATOR.$path : $path);
    }
}

if (!function_exists('logger')) {
    function logger()
    {
        return app('Psr\Log\LoggerInterface');
    }
}

if (!function_exists('debug_log')) {
    /**
     * @signature debug_log(mixed $var)
     * @signature debug_log(string label, mixed $var, array $context = [])
     */
    function debug_log()
    {
        if (!env('APP_DEBUG', false)) {
            return;
        }

        if (func_num_args() < 1) {
            throw new InvalidArgumentException('Missing argument');
        } elseif (func_num_args() == 1) {
            $label = null;
            $var = func_get_arg(0);
            $context = [];
        } elseif (func_num_args() == 2) {
            $label = func_get_arg(0);
            $var = func_get_arg(1);
            $context = [];
        } elseif (func_num_args() >= 3) {
            $label = func_get_arg(0);
            $var = func_get_arg(1);
            $context = func_get_arg(2);
        }

        logger()->debug(($label ? "$label: " : '').print_r($var, true), $context);
    }
}

if (!function_exists('var_log')) {
    /**
     * @signature var_log(array $vars)
     * @signature var_log(string $name, mixed $var)
     */
    function var_log()
    {
        if (func_num_args() < 1) {
            throw new InvalidArgumentException('Missing argument');
        } elseif (func_num_args() == 1) {
            $vars = func_get_arg(0);
        } elseif (func_num_args() == 2) {
            $vars = [func_get_arg(0) => func_get_arg(1)];
        }

        foreach ($vars as $name => $var) {
            logger()->debug("[VAR] $name: ".print_r($var, true), []);
        }
    }
}

if (! function_exists('themeview')) {
    /**
     * Get the evaluated view contents for the given view.
     *
     * @param  string  $view
     * @param  array   $data
     * @param  array   $mergeData
     * @return \Illuminate\View\View
     */
    function themeview($view = null, $data = [], $mergeData = [])
    {
        $factory = Container::getInstance()->make('themes');

        if (func_num_args() === 0) {
            return $factory;
        }

        return $factory->view($view, $data, $mergeData);
    }
}
