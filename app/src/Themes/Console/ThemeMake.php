<?php

namespace Minion\Themes\Console;

use Minion\Themes\Stub;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class ThemeMake extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'theme:make {name} {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Crete a new theme.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = strtolower($this->argument('name'));
        
        if($this->laravel['themes']->exists($name) && !$this->option('force')){
            $this->error('Theme already exists.');
            return;
        }

         $this->generate($name);
    }

    /**
     * [generate description]
     * @param  [type] $name [description]
     * @return [type]       [description]
     */
    public function generate($name)
    {
        $themePath = config('themes.paths.absolute').'/'.$name;
        $this->laravel['files']->copyDirectory(__DIR__.'/stubs/theme', $themePath);
        
        Stub::createFromPath(__DIR__.'/stubs/theme.stub', compact('name'))
            ->saveTo($themePath, 'theme.json');
            
        Stub::createFromPath(__DIR__.'/stubs/welcome.blade.stub', compact('name'))
            ->saveTo($themePath.'/views', 'welcome.blade.php');

        $this->info('Theme created successfully.');
    }
}
