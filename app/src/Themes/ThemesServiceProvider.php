<?php

namespace Minion\Themes;

use Illuminate\Support\ServiceProvider;

class ThemesServiceProvider extends ServiceProvider
{   

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->registerCommands();
        $this->registerServices();
    }

    /**
     * Register the package services.
     *
     * @return void
     */
    protected function registerServices()
    {
        $this->app->bindShared('themes.components', function($app) {
            $blade = $app['view']->getEngineResolver()->resolve('blade')->getCompiler();
            return new Components($app, $blade);
        });

        $this->app->bindShared('themes', function($app) {
            return new Themes($app['files'], $app['config'], $app['view'], new Finder());
        });

        // boot theme register not load in lumen framework
        // $this->app->booting(function($app) {
        //     $app['themes']->register();
        // });
        
        // Alias Facades
        class_alias('Minion\Themes\Facades\Theme', 'Theme');
    }

    /**
     * Register commands.
     */
    protected function registerCommands()
    {
        $this->commands('Minion\Themes\Console\ThemeMake');
    }

    /**
     * Get the services provided by the provider.
     *
     * @return string[]
     */
    public function provides()
    {
        return ['themes', 'themes.components'];
    }

}
