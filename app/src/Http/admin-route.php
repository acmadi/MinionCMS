<?php

/**
 * Admin Routing 
 */

$namespace = 'Minion\Http\Controllers\Admin';
$api = app('Dingo\Api\Routing\Router');

$app->group(['namespace' => $namespace], function() use ($app) {
	$app->get('login', ['uses'=>'AuthController@getLogin','as' => 'auth.login']);
	$app->post('login', ['uses'=>'AuthController@postLogin','as' => 'auth.store']);
	$app->get('logout', ['uses'=>'AuthController@getLogout','as' => 'auth.logout']);
	$app->get('register', ['uses'=>'AuthController@getRegister','as' => 'auth.register']);
	$app->post('register', ['uses'=>'AuthController@postRegister','as' => 'auth.post.register']);
});

// dashboard route 
$app->group([
	'namespace' => $namespace, 
	'prefix' => 'admin', 
	'middleware' => 'auth'], function() use ($app) {

	$app->get('/', ['uses' => 'DashboardController@index', 'as' => 'admin.index']);	
});

// posts modul
$app->group([
	'namespace' => $namespace, 
	'prefix'=>'admin/posts', 
	'middleware' => 'auth'], function() use ($app) {

	$app->get('/', ['uses' => 'PostController@index', 'as' => 'admin.posts.index']);
	$app->get('/create', ['uses' => 'PostController@create', 'as' => 'admin.posts.create']);
	$app->post('/create', ['uses' => 'PostController@store', 'as' => 'admin.posts.store']);
	$app->get('/data', ['uses' => 'PostController@data', 'as' => 'admin.posts.data']);
	$app->post('/search', ['uses' => 'PostController@search', 'as' => 'admin.posts.search']);
	
	$app->get('/{id}/show', ['uses' => 'PostController@show', 'as' => 'admin.posts.show']);
	$app->get('/{id}/edit', ['uses' => 'PostController@edit', 'as' => 'admin.posts.edit']);
	$app->put('/{id}/edit', ['uses' => 'PostController@update', 'as' => 'admin.posts.update']);
	$app->delete('/{id}', ['uses' => 'PostController@destroy', 'as' => 'admin.posts.destroy']);
	$app->get('/{id}/comments', ['uses' => 'PostController@comments', 'as' => 'admin.posts.comments']);
});

$app->group([
	'namespace' => $namespace, 
	'prefix'=>'admin/plugins', 
	'middleware' => 'auth'], function() use ($app) {
	// 
	$app->get('/', ['uses' => 'PluginController@index', 'as' => 'admin.plugins.index']);
});

$api->version('v1', function ($api) {
	$api->get('users', function(){
		return 'users apis';
	});
});