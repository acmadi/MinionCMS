<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$app->get('/', function(){
	return view('welcome');
});

$app->get('/test', function(){
	$post = Minion\Entities\Post::with('taxonomies', 'comments', 'attachment', 'revision')->find(1);
	
});

$app->get('img/{path:.+}', [
    'as' => 'image', 
    'uses' => 'ImageController@show'
]);


$app->get('modal', function(){
	return view('media::modal');
});

// route admin
require_once __DIR__ . '/admin-route.php';

