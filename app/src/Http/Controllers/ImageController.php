<?php 

namespace Minion\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Minion\Routing\Controller;

class ImageController extends Controller
{

	public function show(Request $request, $path)
	{	
		$path = public_path($path);
		$size = explode('x', $request->size);
		$options = $request->get('op',null);
		$img = Image::cache(function($image) use ($path, $size, $options) {
		   	
		   	switch ($options) {
	   			case 'c':
	   				# code...
	   				break;
	   			case 'r':
	   					$image->make($path)->resize($size[0], $size[1], function ($c) {
			                $c->aspectRatio();
			            })->encode('png', 80);
	   				break;
	   			default:
	   					$image->make($path)->fit($size[0],$size[1])->encode('png', 80);
	   				break;
	   		}

		}, 43200, true);

		return $img->response('png');
	}
}