<?php 

namespace Minion\Http\Controllers;

use Minion\Routing\Controller as BaseController;
use Theme;

abstract class Controller extends BaseController
{
	public function __construct()
	{	
		
	}

	/**
	 * [getActiveTheme description]
	 * @return [type] [description]
	 */
	public function getActiveTheme()
	{
		return Theme::getActive();
	}

	/**
	 * [setActiveTheme description]
	 * @param [type] $theme [description]
	 */
	public function setActiveTheme($theme)
	{
		$theme = Theme::setActive($theme);
	}
}
