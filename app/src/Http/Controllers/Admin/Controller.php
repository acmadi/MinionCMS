<?php 

namespace Minion\Http\Controllers\Admin;

use Minion\Routing\Controller as BaseController;
use Theme;

abstract class Controller extends BaseController
{

	public function __construct()
	{
		$this->setActiveTheme('admin');
	}

	/**
	 * [getActiveTheme description]
	 * @return [type] [description]
	 */
	public function getActiveTheme()
	{
		return Theme::getActive();
	}

	/**
	 * [setActiveTheme description]
	 * @param [type] $theme [description]
	 */
	public function setActiveTheme($theme)
	{
		$theme = Theme::setActive($theme);
	}
}
