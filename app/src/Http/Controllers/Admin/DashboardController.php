<?php 

namespace Minion\Http\Controllers\Admin;

use Minion\Entities\Post;


class DashboardController extends Controller
{	
	private $posts;

	public function __construct(Post $posts)
	{
		$this->posts = $posts;
	}
	
	public function index()
	{
		return themeview('dashboard.index');
	}
}