<?php 

namespace Minion\Http\Controllers\Admin;

/**
 * 
 *
 *
 * 
 */

use Carbon\Carbon;
use Illuminate\Http\Request;

class PluginController extends Controller
{
	protected $plugins;

	public function __construct()
	{
		$this->setActiveTheme('admin');
	}

	public function index()
	{
		return themeview('plugins.index');
	}
}