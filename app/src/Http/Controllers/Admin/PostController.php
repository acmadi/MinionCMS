<?php 

namespace Minion\Http\Controllers\Admin;

/**
 * Post Controller 
 *
 * 
 * Author : Abdul Hafidz Anshari
 * Email  : aditans88[at]gmail.com
 */

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Minion\Entities\Post;
use Minion\Entities\TermTaxonomy;

class PostController extends Controller
{
	private $posts;
	private $taxonomy;

	public function __construct(Post $posts, TermTaxonomy $taxonomy)
	{
		$this->posts = $posts;
		$this->taxonomy = $taxonomy;
	}

	public function index()
	{	
		return themeview('posts.index');
	}

	public function data()
	{
		$posts = $this->posts->type('post')->published()
				->latest('post_modified')
				->take(0)->limit(10)
				->get();
		$render = view('posts.lists', compact('posts'))->render();	
		return response()->json([
				'status' 	=> 'success',
				'messages' 	=> null,
				'response_code'	=> 200,
				'data'		=> $render
			], 200);
	}

	public function search(Request $request)
	{	
		$q = $request->get('query');
		$posts = $this->posts->type('post')
				->where('post_title', 'LIKE' , '%'.$q.'%')
				->published()->latest('post_modified')
				->take(0)->limit(10)->get();
		$render = view('posts.lists', compact('posts'))->render();	
		return response()->json([
				'status' 	=> 'success',
				'messages' 	=> null,
				'response_code'	=> 200,
				'data'		=> $render
			], 200);
	}

	public function create()
	{
		$taxonomy = $this->taxonomy;
		return themeview('posts.create', compact('taxonomy'));
	}

	public function store(Request $request)
	{	
		try {

			$validator = Validator::make($request->all(), [
	            'post_title' => 'required',
		        'category'   => 'required'
	        ]);

	        if ($validator->fails()) {
	           	return response()->json([
	                'status'  => 'error',
	                'messages' => $validator->errors(),
	                'response_code'		=> 422,
	                'data' => null
	            ], 422);
	        }

	        $input = $request->only('post_title','post_content', 'post_name', 'post_status', 'post_date', 'post_excerpt');
        	$input['post_type'] = 'post';
	        $input['comment_status'] = $request->get('comment_status', 'closed');
	        $input['ping_status'] = $request->get('ping_status', 'closed');
	        $input['post_name'] = str_slug($request->get('post_name'));
	        $input['post_status'] = 'publish';
	        $input['post_date'] = Carbon::now()->format('Y-m-d H:i:s');
	        $input['post_author'] = 1;

	        $post = $this->posts;
	        $post->fill($input);
	        $post->save();

	        // add tags post
	        if(is_array($request->tags) && !empty($request->tags)){	        	
	        	$post->addTerm($request->tags, 'post_tag');
	        }
	        if(is_array($request->category) && !empty($request->category)){
	        	$post->addTerm($request->category, 'category');
	        }

			return response()->json([
                'status'  => 'success',
                'messages' => 'Post hasben posted',
                'response_code'		=> 200,
                'data' => $post
            ], 200);

		} catch (\Exception $e) {
			return response()->json([
                'status'  => 'error',
                'messages' => $e->getMessage(),
                'response_code'		=> 500,
                'data' => null
            ], 500);
		}
	}

	public function show($id)
	{
		$post = $this->posts->find($id);
		return themeview('posts.show', compact('post'));
	}

	public function edit($id)
	{
		$post = $this->posts->find($id);
		$taxonomy = $this->taxonomy;
		return themeview('posts.edit', compact('post', 'taxonomy'));
	}

	public function update($id, Request $request)
	{			
		try {

			$validator = Validator::make($request->all(), [
	            'post_title' => 'required',
		        'category'   => 'required'
	        ]);

	        if ($validator->fails()) {
	           	return response()->json([
	                'status'  	=> 'error',
	                'messages' 	=> $validator->errors(),
	                'response_code'	=> 422,
	                'data' 		=> null
	            ], 422);
	        }

			$post = $this->posts->find($id);
			$post->fill($request->all());
			$post->save();

			// reset taxonomy
			$post->taxonomies()->detach();
	        if(is_array($request->get('category')) && !empty($request->get('category'))){
	        	$post->addTerm($request->category, 'category');
	        }
	        if(is_array($request->get('tags')) && !empty($request->get('tags'))){	        	
	        	$post->addTerm($request->tags, 'post_tag');
	        }
			
			return response()->json([
                'status' 	=> 'success',
                'messages' 	=> 'Update success...',
                'data' 		=> $post,
                'response_code'=> 200,
            ], 200);

		} catch (\Exception $e) {
			return response()->json([
                'status'  	=> 'error',
                'messages' 	=> $e->getMessage(),
                'data' 		=> null,
                'response_code' => 500
            ], 500);
		}
	}

	public function destroy($id)
	{	
		try {
			$post = $this->posts->find($id);
			$post->delete();
			return response()->json([
                'status'  => 'success',
                'messages' => 'Post hasben delete',
                'data' => null
            ], 200);
		} catch (\Exception $e) {
			return response()->json([
                'status'  => 'error',
                'messages' => $e->getMessage(),
                'data' => null
            ], 500);
		}
	}

	public function comments($id)
	{
		# code...
	}
}