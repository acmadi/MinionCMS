<?php 

/**
 * Post Controller
 *
 * menampilkan post yang telah di input
 *
 * 
 */

namespace Minion\Http\Controllers;


use Minion\Entities\Post;


class PostController extends Controller
{
	
	private $posts;
	
	public function __construct(Post $posts)
	{
		$this->posts = $posts;
		$this->setActiveTheme('bootstrap');
	}

	public function index()
	{	
		$posts = $this->posts->published()->paginate(10);		
		return themeview('welcome', compact('posts'));
	}
}