<?php

namespace Minion\Console\Commands;

use Illuminate\Console\Command;

class InstallCommand extends Command
{
    use EnvironmentTrait;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'minion:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install minion CMS.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // entrance
        $this->logo();

        // title
        $this->info('-- Minion Install --');


        // gather
        $config = $this->gather();

        // deside
        if (!$this->deside()) {
            // cancel
            $this->line('Canceled.');

            return;
        }

        // process
        $this->process($config);

        // done
        $this->line('Done.');
    }

    protected function gather()
    {
        app()->configure('app');

        $config = [
            'language' => config('app.locale'),
            'backend_url' => env('BACKENDURL'),
            'site_url' => env('SITEURL'),
            'site_title' => '',
            'site_description' => 'This is a website.',
            'site_public' => false,
            'admin_username' => '',
            'admin_password' => '',
            'admin_email' => '',
        ];

        $config['language'] = $this->ask('Language', $config['language']);
        if ($config['backend_url'] === null) {
            $config['backend_url'] = $this->ask('Backend URL', 'http://localhost:8000');
        } else {
            $this->info('Backend URL: '.$config['backend_url'].' (.env: BACKENDURL)');
        }
        if ($config['site_url'] === null) {
            $config['site_url'] = $this->ask('Site URL', 'http://localhost:8000');
        } else {
            $this->info('Site URL: '.$config['site_url'].' (.env: SITEURL)');
        }
        $config['site_title'] = $this->ask('Site title', $config['site_title']);
        $config['site_description'] = $this->ask('Site description', $config['site_description']);
        $config['site_public'] = $this->confirm('Site public', $config['site_public']);
        $config['admin_username'] = $this->ask('Admin username', $config['admin_username']);
        $config['admin_password'] = $this->ask('Admin password', $config['admin_password']);
        $config['admin_email'] = $this->ask('Admin email', $config['admin_email']);

        return $config;
    }

    protected function deside()
    {
        return $this->confirm('Are you sure?', true);
    }

    protected function process(array $config)
    {
       dd($config);
    }
}
