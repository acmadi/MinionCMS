<?php

namespace Minion\Console\Commands;

trait EnvironmentTrait
{
    protected function logo()
    {
        $this->info('-*- Minion CMS+ -*-');
        $this->line('  <comment>Minion CMS :</comment> '. 0.1);
        $this->line('  <comment>Base Framework:</comment> '.app()->version());
        $this->info('-*-*-*-*-*-*-*--*-');
        $this->line('');
    }
}
