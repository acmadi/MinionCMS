<?php

namespace Minion\Repositories\Criteria;

use Minion\Repositories\RepositoryInterface;

abstract class Criteria
{
	/**
     * @param $model
     * @param RepositoryInterface $repository
     * @return mixed
     */
    public abstract function apply($model, RepositoryInterface $repository);
}