<?php

namespace Minion\Repositories\Posts;

use Minion\Repositories\BaseRepository;

class PostRepository extends BaseRepository implements PostInterface
{
	/**
     * Specify Model class name
     *
     * @return mixed
     */
    function model()
    {
        return 'Minion\Entities\Post';
    }
}