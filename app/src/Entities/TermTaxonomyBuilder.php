<?php

/**
 * Minion\Entities\TermTaxonomyBuilder
 *
 * @author Junior Grossi <juniorgro@gmail.com>
 */

namespace Minion\Entities;

use Illuminate\Database\Eloquent\Builder;
use Minion\Entities\Term;

class TermTaxonomyBuilder extends Builder
{
    private $category_slug;

    public function posts()
    {
        return $this->with('posts');
    }

    /**
     * Get Category Taxonomy
     * @return Collection
     */
    public function category()
    {
        return $this->where('taxonomy', 'category');
    }

    /**
     * Get only posts from a custo post type
     * 
     * @param string $type
     * @return \Minion\Entities\PostBuilder
     */
    public function type($type)
    {
        return $this->where('taxonomy', $type);
    }
    
    /**
     * Get Tags Taxonomy
     * @return Collection
     */
    public function tags()
    {
        return $this->where('taxonomy', 'post_tag');
    }

    /**
     * Get only posts with a specific slug
     *
     * @param string slug
     * @return \Minion\Entities\PostBuilder
     */
    public function slug( $category_slug=null )
    {
        if( !is_null($category_slug) and !empty($category_slug) ) {
            // set this category_slug to be used in with callback
            $this->category_slug = $category_slug;

            // exception to filter on slug from category
            $exception = function($query) {
                $query->where('slug', '=', $this->category_slug);
            };

            // load term to filter
            return $this->whereHas('term', $exception);
        }

        return $this;
    }
}
