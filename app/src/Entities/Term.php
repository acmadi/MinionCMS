<?php

namespace Minion\Entities;

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Term extends Eloquent
{
    protected $table = 'terms';
    protected $primaryKey = 'term_id';
    protected $fillable = ['name','slug','term_group'];
    public $timestamps = false;
}