<?php

namespace Minion\Entities;

use Illuminate\Database\Eloquent\Model;
use Minion\Entities\Term;

class TermTaxonomy extends Model
{
    protected $table = 'term_taxonomy';
    protected $primaryKey = 'term_taxonomy_id';
    protected $fillable = ['term_id','taxonomy','description', 'parent', 'count'];
    protected $with = array('term');
    public $timestamps = false;

    public function term()
    {
        return $this->belongsTo('Minion\Entities\Term', 'term_id');
    }

    public function parentTerm()
    {
        return $this->belongsTo('Minion\Entities\TermTaxonomy', 'parent');
    }

    public function posts()
    {
        return $this->belongsToMany('Minion\Entities\Post', 'term_relationships', 'term_taxonomy_id', 'object_id');
    }

    /**
     * Overriding newQuery() to the custom TermTaxonomyBuilder with some interesting methods
     *
     * @param bool $excludeDeleted
     * @return Minion\Entities\TermTaxonomyBuilder
     */
    public function newQuery($excludeDeleted = true)
    {
        $builder = new TermTaxonomyBuilder($this->newBaseQueryBuilder());
        $builder->setModel($this)->with($this->with);

        if( isset($this->taxonomy) and !empty($this->taxonomy) and !is_null($this->taxonomy) )
            $builder->where('taxonomy', $this->taxonomy);

        return $builder;
    }

    /**
     * Magic method to return the meta data like the post original fields
     *
     * @param string $key
     * @return string
     */
    public function __get($key)
    {
        if (!isset($this->$key)) {
            if (isset($this->term->$key)) {
                return $this->term->$key;
            }
        }

        return parent::__get($key);
    }
}