<?php

/**
 * Post model
 *
 * @author Junior Grossi <juniorgro@gmail.com>
 */

namespace Minion\Entities;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Minion\Entities\Term;
use Minion\Entities\TermTaxonomy;

class Post extends Eloquent
{   
    use TaxonomyTrait;

    const CREATED_AT = 'post_date';
    const UPDATED_AT = 'post_modified';

    protected $table = 'posts';
    protected $primaryKey = 'ID';
    protected $with = array('meta');
    protected $appends = ['edit_link','delete_link','primary_image'];
    protected $dates = ['post_date', 'post_modified', 'post_date_gmt', 'post_modified_gmt'];
    protected $fillable = ['post_author', 'post_date', 'post_date_gmt', 'post_content', 'post_title', 'post_excerpt', 'post_status', 'comment_status', 'ping_status', 'post_password', 'post_name', 'to_ping', 'pinged', 'post_modified', 'post_modified_gmt', 'post_content_filtered', 'post_parent', 'guid', 'menu_order', 'post_type', 'post_mime_type', 'comment_count'];

    /**
     * Meta data relationship
     *
     * @return Minion\Entities\PostMetaCollection
     */
    public function meta()
    {
        return $this->hasMany('Minion\Entities\PostMeta', 'post_id');
    }

    public function fields()
    {
        return $this->meta();
    }

    /**
     * [author relationship]
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function author()
    {
        return $this->belongsTo('Minion\Entities\User', 'post_author');
    }

    /**
     * Taxonomy relationship
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function taxonomies()
    {
        return $this->belongsToMany('Minion\Entities\TermTaxonomy', 'term_relationships', 'object_id', 'term_taxonomy_id');
    }

    /**
     * Comments relationship
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function comments()
    {
        return $this->hasMany('Minion\Entities\Comment', 'comment_post_ID');
    }

    /**
     * Get attachment
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function attachment()
    {
        return $this->hasMany('Minion\Entities\Post', 'post_parent')->where('post_type', 'attachment');
    }

    /**
     * [getPriamryImageAttribute description]
     * @return [type] [description]
     */
    public function getPrimaryImageAttribute()
    {   
        $image = $this->attachment()->where('post_mime_type', 'image/jpeg');
        if($image->count() > 0)
        {
            return $image->first()->meta->_wp_attached_file;
        }
        return null;
    }

    /**
     * Get revisions from post
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function revision()
    {
        return $this->hasMany('Minion\Entities\Post', 'post_parent')->where('post_type', 'revision');
    }

    /**
     * Overriding newQuery() to the custom PostBuilder with some interesting methods
     *
     * @param bool $excludeDeleted
     * @return Minion\Entities\PostBuilder
     */
    public function newQuery($excludeDeleted = true)
    {
        $builder = new PostBuilder($this->newBaseQueryBuilder());
        $builder->setModel($this)->with($this->with);
        $builder->orderBy('post_date', 'desc');

        if (isset($this->postType) and $this->postType) {
            $builder->type($this->postType);
        }

        if ($excludeDeleted and $this->softDelete) {
            $builder->whereNull($this->getQualifiedDeletedAtColumn());
        }

        return $builder;
    }

    /**
     * [getEditLinkAttribute description]
     * @return [type] [description]
     */
    public function getEditLinkAttribute()
    {
        return route('admin.posts.edit', ['id' => $this->attributes['ID']]);
    }

    /**
     * [getEditLinkAttribute description]
     * @return [type] [description]
     */
    public function getDeleteLinkAttribute()
    {
        return route('admin.posts.destroy', ['id' => $this->attributes['ID']]);
    }

    public function getPostDateAttribute()
    {   
        $data = Carbon::createFromFormat('Y-m-d H:i:s', $this->attributes['post_date']);
        return $data->format('Y/m/d');
    }

    /**
     * Magic method to return the meta data like the post original fields
     *
     * @param string $key
     * @return string
     */
    public function __get($key)
    {
        if (!isset($this->$key)) {
            if (isset($this->meta()->get()->$key)) {
                return $this->meta()->get()->$key;
            }
        }

        return parent::__get($key);
    }

    public function save(array $options = array())
    {
        if (isset($this->attributes[$this->primaryKey])) {
            $this->meta->save($this->attributes[$this->primaryKey]);
        }

        return parent::save($options);
    }

    public function hasMany($related, $foreignKey = null, $localKey = null)
    {
        $foreignKey = $foreignKey ?: $this->getForeignKey();

        $instance = new $related;
        $instance->setConnection($this->getConnection()->getName());

        $localKey = $localKey ?: $this->getKeyName();

        return new HasMany($instance->newQuery(), $this, $instance->getTable().'.'.$foreignKey, $localKey);
    }

    public function belongsToMany($related, $table = null, $foreignKey = null, $otherKey = null, $relation = null)
    {
        if (is_null($relation))
        {
            $relation = $this->getBelongsToManyCaller();
        }

        $foreignKey = $foreignKey ?: $this->getForeignKey();

        $instance = new $related;
        $instance->setConnection($this->getConnection()->getName());

        $otherKey = $otherKey ?: $instance->getForeignKey();

        if (is_null($table))
        {
            $table = $this->joiningTable($related);
        }

        $query = $instance->newQuery();

        return new BelongsToMany($query, $this, $table, $foreignKey, $otherKey, $relation);
    }

}