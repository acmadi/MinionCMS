<?php 

namespace Minion\Entities;

use Minion\Entities\Term;
use Minion\Entities\TermTaxonomy;

trait TaxonomyTrait
{	
	 /**
     *
     * @return Array
     */
    public function getTagsAttribute()
    {
        return $this->taxonomies()->tags()->get()->map(function($item){
            return $item->name;
        })->toArray();
    }

    /**
     * Return array of the tag names related to the current model
     *
     * @return array
     */
    public function tagNames()
    {
        return $this->taxonomies()->tags()->get()->map(function($item){
            return $item->name;
        })->toArray();
    }

    /**
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public function getCategoryAttribute()
    {
        return $this->taxonomies()->category()->get()->map(function($item){
            return $item->name;
        })->toArray();
    }

    /**
     * Return array of the tag names related to the current model
     *
     * @return array
     */
    public function categoryNames()
    {
        return $this->taxonomies()->category()->get()->map(function($item){
            return $item->name;
        })->toArray();
    }

	/**
	 * @param $terms
	 * @param $taxonomy
	 * @param int $parent
	 * @param int $order
	 */
	public function addTerm( $terms, $taxonomy, $parent = 0)
	{
		$terms = $this->makeTermsArray($terms);

		$this->createTaxables( $terms, $taxonomy, $parent);
		$terms = Term::whereIn( 'name', $terms )->lists('term_id')->all();
		if ( count($terms) > 0 ) {
			foreach ( $terms as $term )
			{	
				if ( $this->taxonomies()->where('taxonomy', $taxonomy)->where('term_id', $term)->first() )
					continue;
				$tax = TermTaxonomy::where( 'term_id', $term )->first();
                $tax->count = $tax->count + 1;
                $tax->save();
				$this->taxonomies()->attach($tax->term_taxonomy_id);
			}
			return;
		}
		$this->taxonomies()->detach();
	}

	/**
     * Converts input into array
     *
     * @param $tagName string or array
     * @return array
     */
    private function makeTermsArray($termNames)
    {
        if(is_array($termNames) && count($termNames) == 1) {
            $termNames = $termNames[0];
        }
        
        if(is_string($termNames)) {
            $termNames = explode(',', $termNames);
        } elseif(!is_array($termNames)) {
            $termNames = array(null);
        }
        
        $termNames = array_map('trim', $termNames);

        return $termNames;
    }

    public function createTaxables( $terms, $taxonomy, $parent = 0, $order = 0 )
	{
		$terms = $this->makeTermsArray($terms);
		$this->createTerms( $terms );
		$this->createTaxonomies( $terms, $taxonomy, $parent, $order );
	}

	public function createTerms( array $terms )
	{
		if ( count($terms) === 0 )
			return;
		$found = Term::whereIn( 'name', $terms )->lists('name')->all();
		if ( ! is_array($found) ) $found = array();
		foreach ( array_diff( $terms, $found ) as $term ) {
			Term::firstOrCreate([ 'name' => $term, 'slug' => str_slug($term) ]);
		}
	}

	public function createTaxonomies( array $terms, $taxonomy, $parent = 0 )
	{
		if ( count($terms) === 0 )
			return;
		// only keep terms with existing entries in terms table
		$terms = Term::whereIn( 'name', $terms )->lists('name')->all();
		// create taxonomy entries for given terms
		foreach ( $terms as $term ) {
			TermTaxonomy::firstOrCreate([
				'taxonomy' => $taxonomy,
				'term_id'  => Term::where( 'name', $term )->first()->term_id,
				'parent'   => $parent
			]);
		}
	}
}