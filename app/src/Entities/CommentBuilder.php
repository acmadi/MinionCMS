<?php 

/**
 * Minion\Entities\CommentBuilder
 * 
 * @author Junior Grossi <juniorgro@gmail.com>
 */

namespace Minion\Entities;

use Illuminate\Database\Eloquent\Builder;

class CommentBuilder extends Builder
{
    /**
     * Where clause for only approved comments
     * 
     * @return \Minion\Entities\CommentBuilder
     */
    public function approved()
    {
        return $this->where('comment_approved', 1);
    }

}