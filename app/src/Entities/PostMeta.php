<?php

/**
 * Minion\Entities\PostMeta
 *
 * @author Junior Grossi <juniorgro@gmail.com>
 */

namespace Minion\Entities;

use Illuminate\Database\Eloquent\Model as Eloquent;

class PostMeta extends Eloquent
{
    protected $table = 'postmeta';
    protected $primaryKey = 'meta_id';
    public $timestamps = false;
    protected $fillable = array('meta_key', 'meta_value', 'post_id');

    /**
     * Post relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
        return $this->belongsTo('Minion\Entities\Post');
    }

    /**
     * Override newCollection() to return a custom collection
     *
     * @param array $models
     * @return \Minion\Entities\PostMetaCollection
     */
    public function newCollection(array $models = array())
    {
        return new PostMetaCollection($models);
    }
}