<?php

namespace Minion\Entities;

use Illuminate\Database\Eloquent\Model;

class TermRelationship extends Model
{
    protected $table = 'term_relationships';
    protected $primaryKey = array('object_id', 'term_taxonomy_id');

    public function post()
    {
        return $this->belongsTo('Minion\Entities\Post', 'object_id');
    }

    public function taxonomy()
    {
        return $this->belongsTo('Minion\Entities\TermTaxonomy', 'term_taxonomy_id');
    }
}