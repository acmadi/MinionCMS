<?php

require_once __DIR__.'/../vendor/autoload.php';

Dotenv::load(__DIR__.'/../');

/*
|--------------------------------------------------------------------------
| Create The Application
|--------------------------------------------------------------------------
|
| Here we will load the environment and create the application instance
| that serves as the central piece of this framework. We'll use this
| application as an "IoC" container and router for this framework.
|
*/

$app = new Laravel\Lumen\Application(
	realpath(__DIR__.'/../')
);

$app->withFacades();
$app->withEloquent();

/*
|--------------------------------------------------------------------------
| Register Container Bindings
|--------------------------------------------------------------------------
|
| Now we will register a few bindings in the service container. We will
| register the exception handler and the console kernel. You may add
| your own bindings here if you like or you can make another file.
|
*/

$app->singleton(
    Illuminate\Contracts\Debug\ExceptionHandler::class,
    Minion\Exceptions\Handler::class
);

$app->singleton(
    Illuminate\Contracts\Console\Kernel::class,
    Minion\Console\Kernel::class
);

/*
|--------------------------------------------------------------------------
| Register Middleware
|--------------------------------------------------------------------------
|
| Next, we will register the middleware with the application. These can
| be global middleware that run before and after each request into a
| route or middleware that'll be assigned to some specific routes.
|
*/

$app->middleware([
	Illuminate\Cookie\Middleware\EncryptCookies::class,
	// Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
	Illuminate\Session\Middleware\StartSession::class,
	Illuminate\View\Middleware\ShareErrorsFromSession::class,
	// Laravel\Lumen\Http\Middleware\VerifyCsrfToken::class,
]);

$app->routeMiddleware([
	'csrf'	=> Laravel\Lumen\Http\Middleware\VerifyCsrfToken::class,
	'auth'	=> Minion\Http\Middleware\AuthMiddleware::class,
	'guest' => Minion\Http\Middleware\RedirectIfAuthenticated::class,
]);

/*
|--------------------------------------------------------------------------
| Register Service Providers
|--------------------------------------------------------------------------
|
| Here we will register all of the application's service providers which
| are used to bind services into the container. Service providers are
| totally optional, so you are not required to uncomment this line.
|
*/

$app->register(Minion\Providers\AppServiceProvider::class);
$app->register(Minion\Providers\EventServiceProvider::class);

// $app->configure('tinymce');
// $app->register(Ktquez\Tinymce\TinymceServiceProvider::class);
$app->register(Collective\Html\HtmlServiceProvider::class);
$app->register(Collective\Html\InjectVarsServiceProvider::class);

// image manipulation
$app->configure('image');
$app->configure('imagecache');
$app->register(Intervention\Image\ImageServiceProvider::class);
class_alias(Intervention\Image\Facades\Image::class, 'Image');

// Theme Service Provider
$app->configure('themes');
$app->register(Minion\Themes\ThemesServiceProvider::class);

// Plugin 
$app->configure('plugins');
$app->register(Minion\Plugin\PluginServiceProvider::class);

// Api Reguest
$app->configure('api');
$app->register(Dingo\Api\Provider\LumenServiceProvider::class);

// Hack when php ver 7+
if (starts_with(PHP_VERSION, '7.')) {
    $app->register(Minion\Providers\PHP7ServiceProvider::class);
}

/*
|--------------------------------------------------------------------------
| Load The Application Routes
|--------------------------------------------------------------------------
|
| Next we will include the routes file so that they can all be added to
| the application. This will provide all of the URLs the application
| can respond to, as well as the controllers that may handle them.
|
*/

$app->group(['namespace' => 'Minion\Http\Controllers'], function ($app) {
	require __DIR__.'/../app/src/Http/routes.php';
});

return $app;
